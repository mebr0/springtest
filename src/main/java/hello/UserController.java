package hello;

import hello.exception.EmailExistsException;
import hello.exception.InvalidEmailException;
import hello.exception.PasswordLenghtException;
import hello.model.Role;
import hello.model.User;
import hello.repository.RoleRep;
import hello.repository.UserRep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/users")
public class UserController {

    @Autowired
    private UserRep userRep;

    @Autowired
    private RoleRep roleRep;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(path="/register")
    @ResponseBody
    public String register(@RequestParam String email,
                           @RequestParam String password,
                           @RequestParam String name,
                           @RequestParam(defaultValue = "") String surName ) throws InvalidEmailException,
                                                                PasswordLenghtException,
                                                                EmailExistsException {

        if (password.length() < PasswordLenghtException.LOWER_BOUND ||
                password.length() > PasswordLenghtException.UPPER_BOUND) {
            throw new PasswordLenghtException();
        }
        if (userRep.findByEmail(email) != 0) {
            throw new EmailExistsException();
        }

        User user = new User();

        user.setEmail(email);

        user.setPassword(passwordEncoder.encode(password));

        user.setName(name);
        user.setSurName(surName);

        Role citizen = roleRep.getCitizen();

        if (citizen == null) {
            citizen = new Role("citizen");     // if groups does not have citizen creates it

            roleRep.save(citizen);
        }

        user.getRoles().add(citizen);
        citizen.getUsers().add(user);

        try {
            userRep.save(user);
        }
        catch (Exception e) {
             throw new InvalidEmailException();
        }

        return "Registered!";
    }

    @GetMapping(path = "/login")
    @ResponseBody
    public String login( @RequestParam String email,
                         @RequestParam String password ) {

        if (userRep.findByEmail(email) != 0) {
            String encryptedPassword = userRep.getPassword(email).get(0);

            if (passwordEncoder.matches(password, encryptedPassword)) {
                return "Logged in!";
            }
            else {
                return "Wrong password!";
            }
        }
        else {
            return "Registered email not found!";
        }
    }

    @GetMapping
    @ResponseBody
    public Iterable<User> allUsers() {
        return userRep.findAll();
    }
}
