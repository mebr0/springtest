package hello.repository;

import hello.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRep extends CrudRepository<User, Long> {
//    @Transactional          // qtomic
//    @Modifying              // delete, update, insert
//    @Query(value = " UPDATE user SET name = ?2 WHERE name = ?1 ",
//            nativeQuery = true
//    )
//    void renameByName(String oldName, String newName);

    @Query(value = " SELECT * FROM users WHERE name = ?1", nativeQuery = true)
    List<User> findAllByName(String name);

    @Query(value = " SELECT count(*) FROM users WHERE email = ?1 ", nativeQuery = true)
    int findByEmail(String email);

    @Query(value = " SELECT password FROM users WHERE email = ?1 ", nativeQuery = true)
    List<String> getPassword(String email);
}
