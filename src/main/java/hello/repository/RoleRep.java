package hello.repository;

import hello.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRep extends CrudRepository<Role, Long> {

    @Query(value = " SELECT * FROM roles WHERE name = 'citizen' ", nativeQuery = true)
    Role getCitizen();
}
