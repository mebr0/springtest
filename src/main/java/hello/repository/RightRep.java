package hello.repository;

import hello.model.Right;

import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RightRep extends CrudRepository<Right, Long> {
    
    @Query(value = " SELECT count(*) FROM right_action WHERE right_id = ?1 AND action = ?3 AND " +
                    "group_id IN (SELECT role_id FROM user_roles WHERE user_id = ?2 ) AND sign = 1 ",
                    nativeQuery =  true)
    int checkAccess(Long rightId, Long userId, String action);
}
