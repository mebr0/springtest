package hello;

import hello.model.Right;
import hello.repository.RightRep;

import hello.repository.UserRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/ksk")
public class KSKController {

    @Autowired
    private UserRep userRep;

    @Autowired
    private RightRep rightRep;

    private final Long rightId = new Long(1);

    @GetMapping(path = "/create")
    public @ResponseBody String add(@RequestParam Long r_id,
                                    @RequestParam Long g_id,
                                    @RequestParam Long sign,
                                    @RequestParam String action) {

        Right right = new Right();

        right.setRightId(r_id);
        right.setGroupId(g_id);
        right.setSign(Integer.decode(sign.toString()));
        right.setAction(action);

        rightRep.save(right);

        return "Success!";
    }

    @PostMapping(path = "/edit")
    public @ResponseBody String edit(@RequestParam Long id) {
        String action = "edit";

        if (rightRep.checkAccess(rightId, id, action) != 0) {
            return "Yes";
        }
        else {
            return "No";
        }
    }

    @PostMapping(path = "/delete")
    public @ResponseBody String delete(@RequestParam Long id) {
        String action = "delete";

        if (rightRep.checkAccess(rightId, id, action) != 0) {
            return "Yes";
        }
        else {
            return "No";
        }
    }

    @PostMapping(path = "/create")
    public @ResponseBody String create(@RequestParam Long id) {
        String action = "create";

        if (rightRep.checkAccess(rightId, id, action) != 0) {
            return "Yes";
        }
        else {
            return "No";
        }
    }



}
