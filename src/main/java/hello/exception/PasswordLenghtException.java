package hello.exception;

public class PasswordLenghtException extends Exception {

    private final static String TEMP = "Password must be length between %d and %d!";

    public final static Integer LOWER_BOUND = 8;
    public final static Integer UPPER_BOUND = 54;

    public PasswordLenghtException() {
        super(String.format(TEMP, LOWER_BOUND, UPPER_BOUND));
    }
}
