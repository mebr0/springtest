package hello.exception;

public class InvalidEmailException extends Exception {

    private final static String MESSAGE = "Email must match the mail pattern!";

    public InvalidEmailException() {
        super(MESSAGE);
    }
}
