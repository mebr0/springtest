package hello.exception;

public class EmailExistsException extends Exception {

    private final static String MESSAGE = "Email already exists!";

    public EmailExistsException() {
        super(MESSAGE);
    }
}
